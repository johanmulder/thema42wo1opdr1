Protocol voor vertalingsservice
===============================

Inleiding
---------
Dit document beschrijft het protocol voor de vertalingsservice. Er zijn
een aantal status codes en een aantal commando's beschikbaar.

Statuscodes
-----------
De statuscodes zijn opgesplitst in een aantal reeksen. Het begincijfer van een
reeks heeft een speciale betekenis. De reeksen zijn als volgt:

* 1xx: Informatieve status.
* 2xx: Status met waarschuwing.
* 3xx: Status met fout.

Verder volgt na de statuscode een informatie bericht. Het bericht en de statuscode
zijn gescheiden door een spatie.


Server statusberichten
======================
Wanneer er geconnecteerd wordt naar de server, zal deze antwoorden met
statuscode 100. Wanneer de client de verbinding wil verbreken, zal de
server antwoorden met statuscode 101.
Als een commando niet bestaat, zal deze een status 301 geven.
Als een command ongeldige parameters bevat, zal deze een status 302 geven.


Functionaliteit
===============

Meerdere talen
--------------
Meerdere talen kunnen worden gebruikt in de vertalingsservice. Derhalve moet bij
het vertalen van een woord of zin de bron- en doeltaal worden opgegeven. Deze
taal wordt aangegevens middels de taalcode in vorm <taal>\_<dialect>.

Voorbeelden:

* Fries: nl\_fy
* Nederlands: nl\_nl
* Amerikaans engels: en\_us
* Brits amerikaans: en\_uk


Commandoset
===========

Verbinding verbreken
--------------------
Command: QUIT

Voorbeeld:

* Client: QUIT
* Server: 101 CYA

Vertaling van een woord
-----------------------
Commando: TRW <van taalcode> <naar taalcode> <woord>

Statuscodes:

* 102: Woord vertaald
* 201: Woord niet gevonden in brontaal
* 202: Woord niet gevonden in doeltaal
* 303: Brontaal niet beschikbaar
* 304: Doeltaal niet beschikbaar

Voorbeelden:

* Client: TRW nl\_nl en\_us kleur
* Server: 102 color


* Client: TRW en\_us fr\_fr color
* Server: 102 couleur


* Client: TRW nl\_fy en\_us jitiizer
* Server: 301 nl\_fy taal niet beschikbaar

Vertaling van een zin
---------------------
Command: TRS <van taalcode><naar taalcode> <zin>

Statuscodes:

* 103: Zin compleet vertaald
* 203: Gedeeltelijke vertaling.
* 303: Brontaal niet beschikbaar
* 304: Doeltaal niet beschikbaar

Voorbeelden:

* Client: TRS nl\_nl en\_us Dit is een vertaling
* Server: 103 This is a translation


* Client: TRS nl\_nl en\_us Dit is geen vertaling
* Server: 203 This is geen translation

