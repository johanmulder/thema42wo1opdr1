package nl.hanze.distapps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import nl.hanze.translate.TranslatorFactory;

/**
 * Class for the Translation Server
 * 
 * @author Chakir Bouziane
 * @author Johan Mulder
 * 
 */
public class TranslateServer implements Runnable
{
	private final Socket clientSocket;
	private CommandHandler commandHandler;
	private PrintWriter out = null;

	/**
	 * Constructor for the translatien Server
	 * 
	 * @param clientSocket the Socket of the client
	 * @throws IOException 
	 */
	public TranslateServer(Socket clientSocket, TranslatorFactory translatorFactory) throws IOException
	{
		this.clientSocket = clientSocket;
		out = new PrintWriter(clientSocket.getOutputStream(), true);
		commandHandler = new CommandHandler(out, translatorFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	public void run()
	{
		try
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));

			// Status header.
			printStatus(Status.WELCOME, "Welkom");

			boolean quit = false;
			while (!quit)
			{
				try
				{
					String inputLine = in.readLine();
					if (inputLine == null)
						quit = true;
					else
						quit = commandHandler.handleCommand(CommandParser.parse(inputLine));
				}
				catch (CommandErrorException e)
				{
					printStatus(e.getStatusCode(), e.getMessage());
				}
			}

			// Print final status.
			printStatus(Status.CYA, "Doei");
			clientSocket.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Print a status line with the given message.
	 * @param status
	 * @param message
	 */
	private void printStatus(int status, String message)
	{
		out.println(status + " " + message);
	}

	/**
	 * Main starting method of the server
	 * 
	 * @param args
	 * @throws IOException when it is not able to listen to the given port
	 */
	public static void main(String[] args) throws IOException
	{
		try
		{
			@SuppressWarnings("resource")
			ServerSocket serverSocket = new ServerSocket(Settings.PORT_NUM);
			System.out.println("Server waiting connections on port " + Settings.PORT_NUM);
			while (true)
			{
				try
				{
					Socket clientSocket = serverSocket.accept();
					System.out.println("Connection accepted from " + clientSocket.getRemoteSocketAddress());
					new Thread(new TranslateServer(clientSocket, new TranslatorFactory())).start();
				}
				catch (IOException e)
				{
					System.err.println("Accept failed.");
				}
			}

		}
		catch (IOException e)
		{
			System.err.println("Could not listen on port: " + Settings.PORT_NUM);
			System.exit(1);
		}
	}
}
