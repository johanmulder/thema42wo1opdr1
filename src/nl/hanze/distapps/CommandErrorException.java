package nl.hanze.distapps;

@SuppressWarnings("serial")
public class CommandErrorException extends Exception
{
	private final int statusCode;
	
	public CommandErrorException(int statusCode, String message)
	{
		super(message);
		this.statusCode = statusCode;
	}

	public int getStatusCode()
	{
		return statusCode;
	}
}
