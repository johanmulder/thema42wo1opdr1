package nl.hanze.distapps;

import java.io.*;
import java.net.*;

/**
 * Class for the Client
 * 
 * @author Chakir Bouziane
 * 
 */
public class TranslateClient
{
	Socket translateSocket = null;
	PrintWriter out = null;
	BufferedReader in = null;

	public TranslateClient()
	{
		makeConnection();

		try
		{
			handleRequests();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Method making the connection to the server
	 */
	private void makeConnection()
	{
		try
		{
			translateSocket = new Socket(Settings.HOSTNAME, Settings.PORT_NUM);
			out = new PrintWriter(translateSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(translateSocket.getInputStream()));
		}
		catch (UnknownHostException e)
		{
			System.err.println("Don't know about host: " + Settings.HOSTNAME);
			System.exit(1);
		}
		catch (IOException e)
		{
			System.err.println("Couldn't get I/O for the connection to: " + Settings.HOSTNAME);
			System.exit(1);
		}
	}

	/**
	 * Method handling the requests from the user
	 * 
	 * @throws IOException when it doesn't get an I/O connection to the server
	 */
	private void handleRequests() throws IOException
	{
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String serverInput;
		String userInput;

		while ((serverInput = in.readLine()) != null)
		{
			System.out.println("Server: " + serverInput);
			if (serverInput.startsWith(Integer.toString(Status.CYA)))
				break;

			userInput = stdin.readLine();
			if (userInput != null)
			{
				System.out.println("Client: " + userInput);
				out.println(userInput);
			}
		}

		out.close();
		in.close();
		stdin.close();
		translateSocket.close();
	}

	/**
	 * Main starting method for the Client
	 * 
	 * @param args
	 * @throws IOException When it can´t connect to the socket
	 */
	public static void main(String[] args) throws IOException
	{
		new TranslateClient();

	}
}
