package nl.hanze.distapps;

import java.io.PrintWriter;

import nl.hanze.translate.Language;
import nl.hanze.translate.LanguageNotFoundException;
import nl.hanze.translate.Translator;
import nl.hanze.translate.TranslatorFactory;
import nl.hanze.translate.WordNotFoundException;

/**
 * Handle command data read by the server class.
 * 
 * @author "Johan Mulder"
 */
class CommandHandler
{
	private final PrintWriter out;
	private final TranslatorFactory translatorFactory;

	/**
	 * Command handler constructor.
	 * 
	 * @param out Output stream
	 * @param translatorFactory Factory for creating translators.
	 */
	public CommandHandler(PrintWriter out, TranslatorFactory translatorFactory)
	{
		this.out = out;
		this.translatorFactory = translatorFactory;
	}

	/**
	 * Handle the given command.
	 * 
	 * @param command
	 * @return True if the quit command was entered, false otherwise.
	 */
	public boolean handleCommand(CommandData command) throws CommandErrorException
	{
		// Determine what to do.
		switch (command.getCommand())
		{
			case TRW:
				handleWordTranslation(command);
				break;
			case TRS:
				handleSentenceTranslation(command);
				break;
			case UNKNOWN:
				throw new CommandErrorException(Status.UNKNOWN_COMMAND, "Commando onbekend");

			case QUIT:
				return true;
		}

		return false;
	}

	/**
	 * Print a status and message to the client.
	 * 
	 * @param status
	 * @param message
	 */
	private void printStatus(int status, String message)
	{
		out.println(status + " " + message);
	}

	/**
	 * Get the translator object for the given language.
	 * 
	 * @param language
	 * @return
	 * @throws LanguageNotFoundException
	 */
	private Translator getTranslatorForLanguage(Language language) throws LanguageNotFoundException
	{
		return translatorFactory.getTranslator(language);
	}

	/**
	 * Get the language enum from the string.
	 * 
	 * @param language
	 * @return
	 */
	private Language getLanguage(String language)
	{
		return Language.fromString(language.toUpperCase());
	}

	/**
	 * Validate the parameters for word translation.
	 * 
	 * @param parameters
	 * @throws CommandErrorException
	 */
	private void validateWordTranslationParameters(String[] parameters)
			throws CommandErrorException
	{
		if (parameters.length != 3)
			throw new CommandErrorException(Status.INVALID_PARAMETERS, "3 parameters verwacht, "
					+ parameters.length + " ontvangen");
	}

	/**
	 * Handle word translation.
	 * 
	 * @param command
	 * @throws CommandErrorException
	 */
	private void handleWordTranslation(CommandData command) throws CommandErrorException
	{
		String[] parameters = command.getParameters();
		// Validate the parameters.
		validateWordTranslationParameters(parameters);
		// Translate the word.
		Language srcLanguage = getLanguage(parameters[0]);
		Language dstLanguage = getLanguage(parameters[1]);
		try
		{
			printStatus(Status.WORD_TRANSLATED, getTranslatorForLanguage(srcLanguage)
					.translateWord(parameters[2], dstLanguage));
		}
		catch (WordNotFoundException e)
		{
			int status;
			if (e.getLanguage() == srcLanguage)
				status = Status.WORD_NOT_FOUND_SRC_LANG;
			else
				status = Status.WORD_NOT_FOUND_DST_LANG;
			printStatus(status, "Woord niet gevonden");
		}
		catch (LanguageNotFoundException e)
		{
			printStatus(Status.SRC_LANG_NOT_AVAILABLE, "Taal " + String.valueOf(e.getLanguage())
					+ " niet beschikbaar");
		}
	}

	/**
	 * Validate the parameters for sentence translation.
	 * 
	 * @param parameters
	 * @throws CommandErrorException
	 */
	private void validateSentenceTranslationParameters(String[] parameters)
			throws CommandErrorException
	{
		if (parameters.length <= 3)
			throw new CommandErrorException(Status.INVALID_PARAMETERS,
					"Minstens 3 parameters verwacht, " + parameters.length + " ontvangen");
	}

	/**
	 * Handle the sentence translation.
	 * 
	 * @param command
	 * @throws CommandErrorException
	 */
	private void handleSentenceTranslation(CommandData command) throws CommandErrorException
	{
		String[] parameters = command.getParameters();
		// Validate the parameters.
		validateSentenceTranslationParameters(parameters);

		// Translate the sentence.
		Language srcLanguage = getLanguage(parameters[0]);
		Language dstLanguage = getLanguage(parameters[1]);
		final Translator translator;
		try
		{
			translator = translatorFactory.getTranslator(srcLanguage);
		}
		catch (LanguageNotFoundException e)
		{
			printStatus(Status.SRC_LANG_NOT_AVAILABLE, "Taal " + String.valueOf(e.getLanguage())
					+ " niet beschikbaar");
			return;
		}

		int status = Status.SENTENCE_TRANSLATED;
		StringBuffer sb = new StringBuffer();
		// The first 2 parts of the array contain the languages.
		for (int i = 2; i < parameters.length; i++)
		{
			String word = parameters[i];
			try
			{
				sb.append(translator.translateWord(word, dstLanguage) + " ");
			}
			catch (WordNotFoundException e)
			{
				// If the word is not found in the source language, just
				// add it to the output.
				if (e.getLanguage() == srcLanguage)
					sb.append(word + " ");
				// This is not a complete translation, so set the status
				// accordingly.
				status = Status.PARTIAL_TRANSLATION;
			}
		}

		printStatus(status, sb.toString());
	}

}
