package nl.hanze.distapps;

/**
 * Command enum
 * 
 * @author Johan Mulder
 */
public enum Command
{
	TRW("TRW"), TRS("TRS"), QUIT("QUIT"), UNKNOWN("UNKNOWN");
	private String value;

	private Command(String s)
	{
		value = s;
	}

	/**
	 * Get the string value of the enum.
	 */
	@Override
	public String toString()
	{
		return value;
	}

	/**
	 * Get a command from a string.
	 * 
	 * @param s
	 * @return The enum or UNKNOWN if not found.
	 */
	public static Command fromString(String s)
	{
		if (s != null)
			for (Command c : Command.values())
				if (c.toString().equals(s))
					return c;
		return Command.UNKNOWN;
	}
}
