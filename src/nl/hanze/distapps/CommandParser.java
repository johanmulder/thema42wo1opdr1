package nl.hanze.distapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandParser
{
	public static CommandData parse(String line) throws CommandErrorException
	{
		String[] commandParts = line.split(" ");
		if (commandParts.length == 0)
			throw new CommandErrorException(Status.UNKNOWN_COMMAND, "Commando onbekend");
		
		// Get the command.
		Command command = getCommandFromString(commandParts[0].toUpperCase());
		
		// Get the parameters if available.
		ArrayList<String> parameters = new ArrayList<>(Arrays.asList(commandParts));
		parameters.remove(0);
		// Initialize new command data.
		return new CommandData(command, getParametersFromList(parameters));
	}
	
	private static String[] getParametersFromList(List<String> list)
	{
		String[] parameters = new String[list.size()];
		list.toArray(parameters);
		return parameters;
	}
	
	private static Command getCommandFromString(String s) throws CommandErrorException
	{
		Command command = Command.fromString(s);
		if (command == Command.UNKNOWN)
			throw new CommandErrorException(Status.UNKNOWN_COMMAND, "Commando onbekend");
		return command;
	}
}
