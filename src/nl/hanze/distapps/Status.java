package nl.hanze.distapps;

/**
 * Server status codes.
 * @author Johan Mulder
 *
 */
public class Status
{
	// Informational.
	public static final int WELCOME = 100;
	public static final int CYA = 101;
	public static final int WORD_TRANSLATED = 102;
	public static final int SENTENCE_TRANSLATED = 103;
	// Warnings
	public static final int WORD_NOT_FOUND_SRC_LANG = 201;
	public static final int WORD_NOT_FOUND_DST_LANG = 202;
	public static final int PARTIAL_TRANSLATION = 203;
	// Errors
	public static final int UNKNOWN_COMMAND = 301;
	public static final int INVALID_PARAMETERS = 302;
	public static final int SRC_LANG_NOT_AVAILABLE = 303;
	public static final int DST_LANG_NOT_AVAILABLE = 304;
}
