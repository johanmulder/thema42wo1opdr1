package nl.hanze.distapps;

public class CommandData
{
	private final Command command;
	private final String[] parameters;
	
	public CommandData(Command command, String[] parameters)
	{
		this.command = command;
		this.parameters = parameters;
	}

	public Command getCommand()
	{
		return command;
	}

	public String[] getParameters()
	{
		return parameters;
	}
}
