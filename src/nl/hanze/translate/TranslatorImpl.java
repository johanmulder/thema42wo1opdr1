package nl.hanze.translate;

import java.util.Map;

/**
 * Poor implementation of a translator class.
 * 
 * @author "Johan Mulder"
 */
public class TranslatorImpl implements Translator
{
	private final TranslationDictionary translationDictionary;

	public TranslatorImpl(TranslationDictionary translationDictionary)
	{
		this.translationDictionary = translationDictionary;
	}
	
	@Override
	public String translateWord(String word, Language toLanguage) throws WordNotFoundException
	{
		// Get the dictionary of the destination language.
		Map<Language, String> translationDict = translationDictionary.getDictionaryForWord(word);
		// Get the translated word.
		String translated;
		if ((translated = translationDict.get(toLanguage)) == null)
			throw new WordNotFoundException(word, toLanguage);
		return translated;
	}
}
