package nl.hanze.translate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Translation dictionary class.
 * @author Johan Mulder
 *
 */
public class TranslationDictionary
{
	// The map key is the word in the given language, the value for an entry
	// represents the word in another language.
	private final Map<String, Map<Language, String>> dictionary;
	private final Language language;

	/**
	 * Construct the dictionary class.
	 * @param language
	 * @throws LanguageNotFoundException
	 */
	public TranslationDictionary(Language language) throws LanguageNotFoundException
	{
		dictionary = new HashMap<>();
		this.language = language;
		loadDictionary(language);
	}

	/**
	 * Construct the dictionary class with a predefined dictionary.
	 * @param language The language of the dictionary.
	 * @param dictionary The predefined dictionary.
	 */
	public TranslationDictionary(Language language, Map<String, Map<Language, String>> dictionary)
	{
		this.dictionary = dictionary;
		this.language = language;
	}
	
	/**
	 * Load the dictionary.
	 * @throws LanguageNotFoundException
	 */
	private void loadDictionary(Language sourceLanguage) throws LanguageNotFoundException
	{
		Properties dictFile = new Properties();
		try
		{
			// Load the dictionary from the dictionary.properties file.
			dictFile.load(getClass().getResourceAsStream("dictionary.properties"));
			// Loop through all entries in the dictionary.
			for (Object o : dictFile.keySet())
			{
				// A key contains the source and destination languages, separated by a .
				String[] languages = ((String) o).toUpperCase().split("\\.");
				// A value contains the source and destination word, separated by a |
				String translation = (String) dictFile.get(o);

				// Get the destination language and word for the current entry.
				final Language destLanguage;
				final String translatedWord;
				final String ourWord;
				if (languages[0].equals(String.valueOf(sourceLanguage)))
				{
					// If the first part of the string is the source language, the second
					// part is the destination language and the word in it is our own word.
					destLanguage = Language.fromString(languages[1]);
					translatedWord = translation;
					ourWord = languages[2];
				}
				else if (languages[1].equals(String.valueOf(sourceLanguage)))
				{
					// If the second part of the string is the source language, the
					// first part is the source language and our word is the translated word.
					destLanguage = Language.fromString(languages[0]);
					translatedWord = languages[2];
					ourWord = translation;
				}
				else
					continue;

				// Add the word to the dictionary.
				addWord(ourWord, this.dictionary, destLanguage, translatedWord);
			}
			
			// If no words were added, the source language was not found.
			if (this.dictionary.size() == 0)
				throw new LanguageNotFoundException(sourceLanguage);
		}
		catch (IOException e)
		{
			// If there's an IO exception, there's no language.
			// This isn't really the way to go, but I doubt anyone is ever going to
			// care about it.
			throw new LanguageNotFoundException(sourceLanguage);
		}
	}

	/**
	 * Add a word to the dictionary.
	 * @param word The word to add.
	 * @param dictionary The dictionary to add it to.
	 * @param language The language of the translated word
	 * @param translation The translated word.
	 */
	private void addWord(String word, Map<String, Map<Language, String>> dictionary,
			Language language, String translation)
	{
		// If the word hasn't been seen before, create a new map for this word.
		word = word.toLowerCase();
		final Map<Language, String> destDict; 
		if (!dictionary.containsKey(word))
		{
			destDict = new HashMap<Language, String>();
			dictionary.put(word, destDict);
		}
		else
			destDict = dictionary.get(word);
		
		// Get the destination directory and enter the word.
		dictionary.get(word);
		destDict.put(language, translation.toLowerCase());
	}
	
	/**
	 * Get the translation dictionary for the given word.
	 * @param word
	 * @return
	 * @throws WordNotFoundException
	 */
	public Map<Language, String> getDictionaryForWord(String word) throws WordNotFoundException
	{
		word = word.toLowerCase();
		Map<Language, String> translationDict = dictionary.get(word);
		if (translationDict == null)
			throw new WordNotFoundException(word, language);
		return translationDict;
	}
}
