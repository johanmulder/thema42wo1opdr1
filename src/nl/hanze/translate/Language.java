package nl.hanze.translate;

public enum Language
{
	NL_NL("NL_NL"), EN_US("EN_US"), DE_DE("DE_DE"), UNKNOWN("UNKNOWN");
	private String value;

	private Language(String s)
	{
		value = s;
	}

	@Override
	public String toString()
	{
		return value;
	}

	/**
	 * Get a language from by string.
	 * 
	 * @param s
	 * @return
	 */
	public static Language fromString(String s)
	{
		if (s != null)
			for (Language e : Language.values())
				if (e.toString().equals(s))
					return e;
		return Language.UNKNOWN;
	}
}
