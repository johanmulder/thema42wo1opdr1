package nl.hanze.translate;

public interface Translator
{
	/**
	 * Translate a given word into the given language.
	 * 
	 * @param word
	 * @param toLanguage
	 * @return
	 * @throws WordNotFoundException Thrown if the word was not found in the
	 *             source or destination dictionary.
	 */
	public String translateWord(String word, Language toLanguage) throws WordNotFoundException;
}
