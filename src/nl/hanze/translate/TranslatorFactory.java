package nl.hanze.translate;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory class for creating translator objects.
 * 
 * @author "Johan Mulder"
 */
public class TranslatorFactory
{
	private Map<Language, Translator> translators = new HashMap<>();

	/**
	 * Get a translator for the given language.
	 * 
	 * @param forLanguage
	 * @return
	 * @throws SourceLanguageNotFoundException
	 */
	public Translator getTranslator(Language forLanguage) throws LanguageNotFoundException
	{
		// Initialize the translator for the given language if not done yet.
		if (!translators.containsKey(forLanguage))
			translators
					.put(forLanguage, new TranslatorImpl(new TranslationDictionary(forLanguage)));

		return translators.get(forLanguage);
	}
}
