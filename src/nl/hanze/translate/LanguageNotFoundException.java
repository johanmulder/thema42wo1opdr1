package nl.hanze.translate;

@SuppressWarnings("serial")
public class LanguageNotFoundException extends Exception
{
	private final Language language;
	
	public LanguageNotFoundException(Language language)
	{
		this.language = language;
	}
	
	/**
	 * Get the languages which is not found.
	 * @return
	 */
	public Language getLanguage()
	{
		return language;
	}
}
