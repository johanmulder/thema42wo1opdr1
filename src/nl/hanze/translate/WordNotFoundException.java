package nl.hanze.translate;

@SuppressWarnings("serial")
public class WordNotFoundException extends Exception
{
	private final String word;
	private final Language language;
	
	/**
	 * Exception constructor.
	 * @param word The word which has not been found in the dictionary.
	 * @param language The language in which the word was not found.
	 */
	public WordNotFoundException(String word, Language language)
	{
		this.word = word;
		this.language = language;
	}

	/**
	 * Get the word which was not found in the dictionary.
	 * @return
	 */
	public String getWord()
	{
		return word;
	}

	/**
	 * Get the language in which the word was not found.
	 * @return
	 */
	public Language getLanguage()
	{
		return language;
	}
}
