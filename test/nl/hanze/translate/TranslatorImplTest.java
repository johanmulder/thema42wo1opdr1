package nl.hanze.translate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TranslatorImplTest
{
	private TranslatorImpl translator;

	@Before
	public void setUp()
	{
		// Initialize the translation directory.
		Map<String, Map<Language, String>> dictionary = new HashMap<>();
		// Add a translation for the english word "unit".
		Map<Language, String> translated = new HashMap<>();
		translated.put(Language.NL_NL, "eenheid");
		dictionary.put("unit", translated);
		// And a german word.
		translated = new HashMap<>();
		translated.put(Language.DE_DE, "sprache");
		dictionary.put("language", translated);
		// Create the translation dictionary and the translator.
		TranslationDictionary td = new TranslationDictionary(Language.EN_US, dictionary);
		translator = new TranslatorImpl(td);
	}

	/**
	 * Test a regular translation.
	 * 
	 * @throws WordNotFoundException
	 */
	@Test
	public void testTranslate() throws WordNotFoundException
	{
		assertEquals("eenheid", translator.translateWord("unit", Language.NL_NL));
	}

	/**
	 * Expect a WordNotFoundException when a non-existent word has been given.
	 */
	@Test
	public void testNonExistingWordFromSourceLanguage()
	{
		try
		{
			translator.translateWord("none", Language.NL_NL);
		}
		catch (WordNotFoundException e)
		{
			// The word "none" is not added in the dictionary, so this should be
			// true.
			assertEquals(Language.EN_US, e.getLanguage());
			return;
		}
		fail("Unexpected point reached");
	}

	/**
	 * Expect a WordNotFoundException when a non-existent word has been given.
	 */
	@Test
	public void testNonExistingWordFromDestinationLanguage()
	{
		try
		{
			translator.translateWord("language", Language.NL_NL);
		}
		catch (WordNotFoundException e)
		{
			// The NL_NL translation directory contains no translation for
			// "language", so it should fail.
			assertEquals(Language.NL_NL, e.getLanguage());
			return;
		}
		fail("Unexpected point reached");
	}
}
