package nl.hanze.translate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class TranslationDictionaryTest
{
	private TranslationDictionary dictionary;
	
	@Before
	public void setUp() throws LanguageNotFoundException
	{
		dictionary = new TranslationDictionary(Language.EN_US);
	}

	/**
	 * Test a lookup and verify the result.
	 * @throws WordNotFoundException
	 */
	@Test
	public void testLookup() throws WordNotFoundException
	{
		Map<Language, String> nlDict = dictionary.getDictionaryForWord("tree");
		assertNotNull(nlDict);
		assertEquals("boom", nlDict.get(Language.NL_NL));
	}
	
	/**
	 * Test the lookup of a nonexistent word.
	 */
	@Test
	public void testNonExistentWord()
	{
		try
		{
			dictionary.getDictionaryForWord("none");
		}
		catch (WordNotFoundException e)
		{
			assertEquals(e.getLanguage(), Language.EN_US);
			return;
		}
		fail("Unexpected point reached");
	}

}
